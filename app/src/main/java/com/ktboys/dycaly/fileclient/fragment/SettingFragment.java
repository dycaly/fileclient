package com.ktboys.dycaly.fileclient.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ktboys.dycaly.fileclient.R;
import com.ktboys.dycaly.fileclient.activity.SettingEditActivity;
import com.ktboys.dycaly.fileclient.utils.SharedPreferencesHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class SettingFragment extends Fragment {

    @Bind(R.id.fg_setting_server_right)
    ImageView fgSettingServerRight;
    @Bind(R.id.fg_setting_server_value)
    TextView fgSettingServerValue;
    @Bind(R.id.fg_setting_server_layout)
    LinearLayout fgSettingServerLayout;
    @Bind(R.id.fg_setting_folder_right)
    ImageView fgSettingFolderRight;
    @Bind(R.id.fg_setting_folder_value)
    TextView fgSettingFolderValue;
    @Bind(R.id.fg_setting_folder_layout)
    LinearLayout fgSettingFolderLayout;

    private String server = "";
    private String folder = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_setting, container, false);
        ButterKnife.bind(this, view);
        refreshView();
        return view;
    }

    /**
     * 刷新UI数据:
     *      1. 服务器地址
     *      2. 工作目录
     */
    private void refreshView() {
        server = (String) SharedPreferencesHelper.getSharedPreferences(getActivity(), "server", "");
        folder = (String) SharedPreferencesHelper.getSharedPreferences(getActivity(), "folder", "");
        fgSettingServerValue.setText(server);
        fgSettingFolderValue.setText(folder);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * 点击事件
     * @param view
     */
    @OnClick({R.id.fg_setting_server_layout, R.id.fg_setting_folder_layout})
    public void onViewClicked(View view) {
        Intent intent;
        Bundle bundle;
        switch (view.getId()) {

            //点击服务器地址，进入修改
            case R.id.fg_setting_server_layout:
                intent = new Intent(getActivity(), SettingEditActivity.class);
                bundle = new Bundle();
                bundle.putString("title", "服务地址");
                bundle.putString("type", "server");
                bundle.putString("server", server);
                intent.putExtras(bundle);
                startActivityForResult(intent, 0);
                break;

            //点击工作目录，进入修改
            case R.id.fg_setting_folder_layout:
                intent = new Intent(getActivity(), SettingEditActivity.class);
                bundle = new Bundle();
                bundle.putString("title", "工作目录");
                bundle.putString("type", "folder");
                bundle.putString("folder", folder);
                intent.putExtras(bundle);
                startActivityForResult(intent, 1);
                break;
        }
    }

    /**
     * 页面跳转返回后处理函数
     * @param requestCode 请回码
     * @param resultCode 返回码,若为0，则表示数据有更新，需要刷新UI
     * @param data 返回数据
     */
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == 0) {
            refreshView();
        }
    }
}
