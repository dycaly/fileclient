package com.ktboys.dycaly.fileclient.network;

import android.content.Context;
import com.ktboys.dycaly.fileclient.utils.MHttps;
import com.ktboys.dycaly.fileclient.utils.SharedPreferencesHelper;
import okhttp3.OkHttpClient;
import retrofit2.Retrofit;


/**
 * Created by dycaly on 2016/6/27.
 */
public class NetRequest {
    private static Retrofit retrofit = null;
    public static OkHttpClient.Builder okBuilder = null;
    /**
     * 获取NetService对象
     * @param context
     * @return
     */
    public static NetService getService(Context context){
        if (retrofit == null){
            retrofit = reCreate(context);
        }
        NetService service = retrofit.create(NetService.class);
        return service;
    }

    /**
     * 创建Retrofit对象
     * @param context
     * @return
     */
    private static Retrofit reCreate(Context context){
        String url = (String) SharedPreferencesHelper.getSharedPreferences(context,"server","");
        Retrofit r = new Retrofit.Builder()
                .client(MHttps.getUnsafeOkHttpClient())
                .baseUrl(url)
                .build();
        return r;
    }
    public static void clearRetrofit(){
        retrofit = null;
    }
}
