package com.ktboys.dycaly.fileclient.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.ktboys.dycaly.fileclient.R;
import com.ktboys.dycaly.fileclient.adapter.FileAdapter;
import com.ktboys.dycaly.fileclient.dataitem.FileItem;
import com.ktboys.dycaly.fileclient.network.NetRequest;
import com.ktboys.dycaly.fileclient.utils.FileHelper;
import com.ktboys.dycaly.fileclient.utils.SharedPreferencesHelper;

import java.io.File;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class UploadFragment extends Fragment {


    @Bind(R.id.upload_listView)
    PullToRefreshListView uploadListView;
    private FileAdapter mFileAdapter;
    private SweetAlertDialog mSweetDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_upload, container, false);
        ButterKnife.bind(this, view);

        initViw();
        refreshData();
        return view;
    }

    /**
     * 初始化View：
     *      1. 设置List的Adapter
     *      2. 设置下拉时更新List的数据
     *      3. 设置点击每个List的时候弹出下载确认对话框
     */
    private void initViw() {
        mFileAdapter = new FileAdapter(getActivity());
        uploadListView.setAdapter(mFileAdapter);
        uploadListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        uploadListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                uploadListView.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshData();
                        uploadListView.onRefreshComplete();
                    }
                });
            }
        });
        uploadListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final FileItem item = mFileAdapter.getItem((int) l);

                mSweetDialog = new SweetAlertDialog(getActivity())
                        .setTitleText("确认上传？")
                        .setContentText(item.getFileName())
                        .setCancelText("取消")
                        .setConfirmText("上传")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                sDialog.setTitleText("正在上传...")
                                        .showCancelButton(false);
                                uploadFile(item.getAbsolutePath());
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        });
                mSweetDialog.show();
            }
        });
    }

    /**
     * 刷新UI：
     *      1. 本地文件列表
     */
    private void refreshData() {
        String rootPath = (String) SharedPreferencesHelper.getSharedPreferences(getActivity(), "folder", "");
        ArrayList<FileItem> fileItems = FileHelper.getFileItems(rootPath);
        mFileAdapter.setDatas(fileItems);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    /**
     * 上传文件
     * @param filePath 文件路径
     */
    public void uploadFile(String filePath) {

        //封装要上传的文件
        File file = new File(filePath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);
        MultipartBody.Part body = MultipartBody.Part.createFormData("uploadfile", file.getName(), requestFile);

        //发起文件请求
        NetRequest.getService(getActivity()).uploadFile(body)
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {

                        //请求成功后弹出提示对话框
                        mSweetDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
                        mSweetDialog.setTitleText("上传成功")
                                .setConfirmText("确定")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        mSweetDialog.dismissWithAnimation();
                                    }
                                });
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        //请求失败后弹出对话框
                        Log.e("Upload error:", t.getMessage());
                        mSweetDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        mSweetDialog.setTitleText("上传失败")
                                .setConfirmText("确定")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        mSweetDialog.dismissWithAnimation();
                                    }
                                });
                    }
                });

    }
}
