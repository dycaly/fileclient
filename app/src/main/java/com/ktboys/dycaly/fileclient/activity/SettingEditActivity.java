package com.ktboys.dycaly.fileclient.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ktboys.dycaly.fileclient.R;
import com.ktboys.dycaly.fileclient.network.NetRequest;
import com.ktboys.dycaly.fileclient.utils.SharedPreferencesHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingEditActivity extends Activity {

    @Bind(R.id.enl_return)
    ImageView enlReturn;
    @Bind(R.id.nbl_title)
    TextView nblTitle;
    @Bind(R.id.nbl_save)
    TextView nblSave;
    @Bind(R.id.setting_input)
    EditText settingInput;
    private String title = "";
    private String type = "";
    private String value = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting_edit);
        ButterKnife.bind(this);


        //获取传递管来的参数
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        title = bundle.getString("title");
        type = bundle.getString("type");
        value = bundle.getString(type);

        //设置标题和默认value
        nblTitle.setText(title);
        settingInput.setText(value);

    }

    /**
     * 点击事件
     * @param view
     */
    @OnClick({R.id.enl_return, R.id.nbl_save})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //点击返回
            case R.id.enl_return:
                setResult(1);
                finish();
                break;
            //点击保存
            case R.id.nbl_save:
                //修改储存的对应Value
                SharedPreferencesHelper.putSharedPreferences(this,type,settingInput.getText().toString());
                //如果修改了服务器地址，清除静态的Retrofit对象，下次使用网络时会重新创建基于新服务器地址的Retrofit
                if(type.equals("server")){
                    NetRequest.clearRetrofit();
                }
                setResult(0);
                finish();
                break;
        }
    }
}
