package com.ktboys.dycaly.fileclient.dataitem;

/**
 * Created by dycaly on 2017/4/5.
 */

public class FileItem {
    private String fileName;
    private String fileSize;
    private String absolutePath;

    public FileItem(String fileName, String fileSize, String absolutePath) {
        this.fileName = fileName;
        this.fileSize = fileSize;
        this.absolutePath = absolutePath;
    }

    public String getFileName() {
        return fileName;
    }

    public String getFileSize() {
        return fileSize;
    }

    public String getAbsolutePath() {
        return absolutePath;
    }
}
