package com.ktboys.dycaly.fileclient;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ktboys.dycaly.fileclient.fragment.DownloadFragment;
import com.ktboys.dycaly.fileclient.fragment.SettingFragment;
import com.ktboys.dycaly.fileclient.fragment.UploadFragment;
import com.ktboys.dycaly.fileclient.utils.FileHelper;
import com.ktboys.dycaly.fileclient.utils.SharedPreferencesHelper;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends FragmentActivity {

    @Bind(R.id.nbl_title)
    TextView nblTitle;
    @Bind(R.id.btl_upload_img)
    ImageView btlUploadImg;
    @Bind(R.id.btl_upload_title)
    TextView btlUploadTitle;
    @Bind(R.id.btl_upload_layout)
    RelativeLayout btlUploadLayout;
    @Bind(R.id.btl_download_img)
    ImageView btlDownloadImg;
    @Bind(R.id.btl_download_title)
    TextView btlDownloadTitle;
    @Bind(R.id.btl_download_layout)
    RelativeLayout btlDownloadLayout;
    @Bind(R.id.btl_setting_img)
    ImageView btlSettingImg;
    @Bind(R.id.btl_setting_title)
    TextView btlSettingTitle;
    @Bind(R.id.btl_setting_layout)
    RelativeLayout btlSettingLayout;

    private UploadFragment mUploadFragment;
    private DownloadFragment mDownloadFragment;
    private SettingFragment mSettingFragment;
    private Fragment mCurrentFragment;
    private FragmentManager mFragmentMan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        initView();

        //判断是否储存了server，folder参数，如果没有，设置默认
        String server = (String) SharedPreferencesHelper.getSharedPreferences(this,"server","");
        if (server.isEmpty()){
            SharedPreferencesHelper.putSharedPreferences(this,"server","https://192.168.26.172:8080/");
        }
        String folder = (String) SharedPreferencesHelper.getSharedPreferences(this,"folder","");
        if (folder.isEmpty()){
            SharedPreferencesHelper.putSharedPreferences(this,"folder", FileHelper.getSdCardPath()+"/"+"FileClient");
        }

    }

    /**
     * 初始化View：
     *      1. 获取FragmentManager管理Fragment
     *      2. 创建3个Fragment
     *      3. 切换到第一个Fragment
     */
    private void initView() {
        mFragmentMan = getSupportFragmentManager();
        mUploadFragment = new UploadFragment();
        mDownloadFragment = new DownloadFragment();
        mSettingFragment = new SettingFragment();
        switchFragment(mUploadFragment);
    }


    /**
     * 页面点击事件
     * @param view
     */
    @OnClick({R.id.btl_upload_layout, R.id.btl_download_layout, R.id.btl_setting_layout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            //点击上传页
            case R.id.btl_upload_layout:
                setSelectedImgColor(btlUploadImg, btlDownloadImg, btlSettingImg);
                setSelectedTextColor(btlUploadTitle, btlDownloadTitle, btlSettingTitle);
                switchFragment(mUploadFragment);
                //设置顶部NavBar标题
                nblTitle.setText("本地文件");
                break;
            //点击下载页
            case R.id.btl_download_layout://点击下载页
                setSelectedImgColor(btlDownloadImg, btlUploadImg, btlSettingImg);
                setSelectedTextColor(btlDownloadTitle, btlUploadTitle, btlSettingTitle);
                switchFragment(mDownloadFragment);
                nblTitle.setText("云端文件");
                break;
            //点击设置页
            case R.id.btl_setting_layout://点击设置页
                setSelectedImgColor(btlSettingImg, btlUploadImg, btlDownloadImg);
                setSelectedTextColor(btlSettingTitle, btlUploadTitle, btlDownloadTitle);
                switchFragment(mSettingFragment);
                nblTitle.setText("设置");
                break;
        }
    }

    /**
     * 设置选中Tab的ICON颜色
     * @param selectedImage 选中的ICON，将设为主题色
     * @param otherImage 其他的ICON，将设为默认色
     */
    public void setSelectedImgColor(ImageView selectedImage, ImageView... otherImage) {
        selectedImage.setColorFilter(ContextCompat.getColor(this, R.color.themeColor));
        for (ImageView imageView : otherImage) {
            imageView.setColorFilter(ContextCompat.getColor(this, R.color.textColorLight));
        }
    }

    /**
     * 设置选中Tab的文字颜色
     * @param selectedText 选中的文字，将设为主题色
     * @param otherText 其他文字，将设为默认色
     */
    public void setSelectedTextColor(TextView selectedText, TextView... otherText) {
        selectedText.setTextColor(ContextCompat.getColor(this, R.color.themeColor));
        for (TextView textView : otherText) {
            textView.setTextColor(ContextCompat.getColor(this, R.color.textColorLight));
        }
    }

    /**
     * 切换Tab页
     * @param to 要切换到的Fragment
     */
    public void switchFragment(Fragment to) {
        if (mCurrentFragment == null) {
            FragmentTransaction transaction = mFragmentMan.beginTransaction();
            transaction.add(R.id.main_framelayout, to).show(to).commit();
            mCurrentFragment = to;
        } else if (mCurrentFragment != to) {
            FragmentTransaction transaction = mFragmentMan.beginTransaction();
            if (!to.isAdded()) {
                transaction.hide(mCurrentFragment).add(R.id.main_framelayout, to).show(to).commit();
            } else {
                transaction.hide(mCurrentFragment).show(to).commit();
            }
            mCurrentFragment = to;
        }
    }

}
