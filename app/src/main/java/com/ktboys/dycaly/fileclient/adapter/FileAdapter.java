package com.ktboys.dycaly.fileclient.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.ktboys.dycaly.fileclient.R;
import com.ktboys.dycaly.fileclient.dataitem.FileItem;

import java.util.ArrayList;

/**
 * Created by dycaly on 2017/4/5.
 */

public class FileAdapter extends BaseAdapter {

    private ArrayList<FileItem> datas;
    private Context context;

    /**
     * 构造方法
     * @param context 上下文，用于获取layout
     */
    public FileAdapter(Context context) {
        this.context = context;
        datas = new ArrayList<>();
    }

    /**
     * 添加Item并更新UI
     * @param item 要添加的Item
     */
    public void addItem(FileItem item) {
        datas.add(item);
        notifyDataSetChanged();
    }

    /**
     * 设置Item数据集
     * @param datas
     */
    public void setDatas(ArrayList<FileItem> datas) {
        this.datas = datas;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return datas.size();
    }

    @Override
    public FileItem getItem(int i) {
        return datas.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    /**
     * 渲染列表时调用,Adapter最重要的方法
     * @param i
     * @param view
     * @param viewGroup
     * @return 返回的View用于渲染List的每一个Cell
     */
    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        /**
         * 定义ViewHolder用于储存已获取的list的控件，并存储到view中，防止重复获取导致内存溢出
         */
        ViewHolder holder = null;
        if (view == null) {
            holder = new ViewHolder();
            view = LayoutInflater.from(context).inflate(R.layout.file_item_list_cell, null);
            holder.nameText = (TextView) view.findViewById(R.id.filc_name);
            holder.sizeText = (TextView) view.findViewById(R.id.filc_size);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        FileItem item = datas.get(i);
        holder.nameText.setText(item.getFileName());
        holder.sizeText.setText(item.getFileSize());
        return view;
    }

    class ViewHolder {
        TextView nameText;
        TextView sizeText;
    }
}
