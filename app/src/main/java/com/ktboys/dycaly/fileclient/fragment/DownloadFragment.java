package com.ktboys.dycaly.fileclient.fragment;


import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.ktboys.dycaly.fileclient.R;
import com.ktboys.dycaly.fileclient.adapter.FileAdapter;
import com.ktboys.dycaly.fileclient.dataitem.DownloadItem;
import com.ktboys.dycaly.fileclient.dataitem.FileItem;
import com.ktboys.dycaly.fileclient.network.NetRequest;
import com.ktboys.dycaly.fileclient.utils.FileHelper;
import com.ktboys.dycaly.fileclient.utils.SharedPreferencesHelper;

import org.json.JSONArray;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import cn.pedant.SweetAlert.SweetAlertDialog;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.content.ContentValues.TAG;


public class DownloadFragment extends Fragment {

    @Bind(R.id.download_listView)
    PullToRefreshListView downloadListView;

    private FileAdapter mFileAdapter;
    private SweetAlertDialog mSweetDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_download, container, false);
        ButterKnife.bind(this, view);

        initViw();
        refreshData();
        return view;
    }

    /**
     * 初始化View：
     *      1. 设置List的Adapter
     *      2. 设置下拉时更新List的数据
     *      3. 设置点击每个List的时候弹出下载确认对话框
     */
    private void initViw() {

        mFileAdapter = new FileAdapter(getActivity());
        downloadListView.setAdapter(mFileAdapter);
        downloadListView.setMode(PullToRefreshBase.Mode.PULL_FROM_START);
        downloadListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener<ListView>() {
            @Override
            public void onRefresh(PullToRefreshBase<ListView> refreshView) {
                downloadListView.post(new Runnable() {
                    @Override
                    public void run() {
                        refreshData();
                    }
                });
            }
        });

        downloadListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                final FileItem item = mFileAdapter.getItem((int) l);
                //弹出对话框
                mSweetDialog = new SweetAlertDialog(getActivity())
                        .setTitleText("确认下载？")
                        .setContentText(item.getFileName())
                        .setCancelText("取消")
                        .setConfirmText("下载")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                sDialog.changeAlertType(SweetAlertDialog.PROGRESS_TYPE);
                                sDialog.setTitleText("正在下载...")
                                        .showCancelButton(false);
                                downloadFile(item.getFileName());
                            }
                        })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.dismissWithAnimation();
                            }
                        });
                mSweetDialog.show();
            }
        });

    }

    /**
     * 下载云端的文件列表，并更新UI
     */
    private void refreshData() {
        Log.i("dowoload list","test");
        //发起获取服务器文件列表的请求
        NetRequest.getService(getActivity()).getFileList()
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {

                        //请求成功之后利用GSON将String转为对象，并更新UI
                        try {
                            String rltStr = new String(response.body().bytes());
                            Gson gson = new Gson();
                            JsonParser parser = new JsonParser();
                            JsonArray Jarray = parser.parse(rltStr).getAsJsonArray();
                            ArrayList<FileItem> fileItems = new ArrayList<>();
                            for (JsonElement obj : Jarray) {
                                DownloadItem item = gson.fromJson(obj, DownloadItem.class);
                                fileItems.add(new FileItem(item.getName(), item.getSize(), ""));
                            }
                            mFileAdapter.setDatas(fileItems);
                        } catch (Exception e) {
                            Log.e("File List Error",e.getMessage());
                        }
                        downloadListView.onRefreshComplete();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable throwable) {
                        Log.e("F", throwable.getMessage());
                        //请求失败后弹出失败提示对话框
                        throwable.printStackTrace();
                        mSweetDialog = new SweetAlertDialog(getActivity(), SweetAlertDialog.ERROR_TYPE)
                                .setTitleText("失败！")
                                .setContentText("更新文件列表")
                                .setConfirmText("确定")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sDialog) {
                                        sDialog.dismissWithAnimation();
                                    }
                                });
                        mSweetDialog.show();
                        downloadListView.onRefreshComplete();
                    }
                });
    }

    /**
     * 下载文件
     * @param filename 文件名
     */
    private void downloadFile(final String filename) {

        //发起下载文件请求
        String server = (String) SharedPreferencesHelper.getSharedPreferences(getActivity(), "server", "");
        String url = server + "download" + "/" + filename;
        NetRequest.getService(getActivity()).downloadFile(url)

                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           final Response<ResponseBody> response) {
                        //请求成功之后保存文件
                        saveFile(response.body(), filename);
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        //请求失败之后弹出失败对话框
                        Log.e("Download error:", t.getMessage());
                        mSweetDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
                        mSweetDialog.setTitleText("下载失败")
                                .setConfirmText("确定")
                                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                                    @Override
                                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                                        mSweetDialog.dismissWithAnimation();
                                    }
                                });
                    }
                });

    }

    /**
     * 保存文件
     * @param body 请求回来的数据
     * @param filename 文件名
     */
    private void saveFile(ResponseBody body, String filename) {
        try {
            String folder = (String) SharedPreferencesHelper.getSharedPreferences(getActivity(), "folder", "");
            File futureStudioIconFile = new File(folder + "/" + filename);
            InputStream inputStream = null;
            OutputStream outputStream = null;
            byte[] fileReader = new byte[4096];
            long fileSize = body.contentLength();
            long fileSizeDownloaded = 0;
            inputStream = body.byteStream();
            outputStream = new FileOutputStream(futureStudioIconFile);
            while (true) {
                int read = inputStream.read(fileReader);

                if (read == -1) {
                    break;
                }

                outputStream.write(fileReader, 0, read);

                fileSizeDownloaded += read;

                Log.d(TAG, "file download: " + fileSizeDownloaded + " of " + fileSize);
            }
            outputStream.flush();
            if (inputStream != null) {
                inputStream.close();
            }
            if (outputStream != null) {
                outputStream.close();
            }

            //保存文件完成后弹出提示对话框
            mSweetDialog.changeAlertType(SweetAlertDialog.SUCCESS_TYPE);
            mSweetDialog.setTitleText("下载成功")
                    .setConfirmText("确定")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            mSweetDialog.dismissWithAnimation();
                        }
                    });

        } catch (IOException e) {

            //保存文件失败后弹出提示对话框
            Log.e("Downlaod Error", e.getMessage());
            mSweetDialog.changeAlertType(SweetAlertDialog.ERROR_TYPE);
            mSweetDialog.setTitleText("下载失败")
                    .setConfirmText("确定")
                    .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                        @Override
                        public void onClick(SweetAlertDialog sweetAlertDialog) {
                            mSweetDialog.dismissWithAnimation();
                        }
                    });

        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }
}
