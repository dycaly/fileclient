package com.ktboys.dycaly.fileclient.utils;

import android.os.Environment;
import android.util.Log;

import com.ktboys.dycaly.fileclient.dataitem.FileItem;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by dycaly on 2017/4/5.
 */

public class FileHelper {

    /**
     * 获取目录下的文件列表
     * @param path 目录
     * @return 文件列表
     */
    public static ArrayList<FileItem> getFileItems(String path){
        ArrayList<FileItem> fileItems = new ArrayList<>();
        File rootFile = new File(path);

        //文件夹不存在则创建
        if(!rootFile.exists()){
            Log.e("Create Folder",path);
            rootFile.mkdirs();
        }

        //遍历文件夹
        if(rootFile.isDirectory()){
            File files[] = rootFile.listFiles();
            if (files != null) {
                for(File file:files){
                    if(file.isFile()){
                        String name = file.getName();
                        String size = FileSizeUtil.getFileOrFilesSize(file.getAbsolutePath(),FileSizeUtil.SIZETYPE_KB)+"KB";
                        String absolutePath = file.getAbsolutePath();
                        fileItems.add(new FileItem(name,size,absolutePath));
                    }
                }
            }

        }
        return fileItems;
    }

    /**
     * 获取储存卡路径
     * @return
     */
    public static String getSdCardPath() {
        boolean exist = isSdCardExist();
        String sdpath = "";
        if (exist) {
            sdpath = Environment.getExternalStorageDirectory().getAbsolutePath();
        }
        return sdpath;

    }

    /**
     * 判断储存卡是否存在
     * @return
     */
    public static boolean isSdCardExist() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

}
