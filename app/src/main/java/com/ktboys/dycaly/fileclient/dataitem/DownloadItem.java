package com.ktboys.dycaly.fileclient.dataitem;

/**
 * Created by dycaly on 2017/4/8.
 */

public class DownloadItem {
    private String name;
    private String size;

    public DownloadItem(String name, String size) {
        this.name = name;
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public String getSize() {
        return size;
    }
}
