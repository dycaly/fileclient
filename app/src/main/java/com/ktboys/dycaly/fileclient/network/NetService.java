package com.ktboys.dycaly.fileclient.network;


import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

/**
 * Created by dycaly on 2016/6/27.
 */
public interface NetService {

    /**
     * 文件上传请求
     * @param file
     * @return
     */
    @Multipart
    @POST("upload")
    Call<ResponseBody> uploadFile(@Part MultipartBody.Part file);

    /**
     * 获取文件列表请求
     * @return
     */
    @GET("filelist")
    Call<ResponseBody> getFileList();

    /**
     * 文件下载请求
     * @param url
     * @return
     */
    @GET
    Call<ResponseBody> downloadFile(@Url String url);

}
